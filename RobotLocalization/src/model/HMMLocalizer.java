package model;

import control.EstimatorInterface;

public class HMMLocalizer implements EstimatorInterface {
	
	private int rows, cols, head;
	private Robot robot;
	private double f[];
	private int[] current_reading;
	
	public HMMLocalizer(int rows, int cols, int head) {
		this.rows = rows;
		this.cols = cols;
		this.head = head;
		robot = new Robot(rows, cols, head);
		f = new double[rows*cols*head];
		for(int i = 0; i < rows*cols*head; i++){
			f[i] =  1.0 / (rows*cols*head);
		}
	}	
	@Override
	public int getNumRows() {
		return rows;
	}

	@Override
	public int getNumCols() {
		return cols;
	}

	@Override
	public int getNumHead() {
		return head;
	}

/*
	 * should trigger one step of the estimation, i.e., true position, sensor reading and 
	 * the probability distribution for the position estimate should be updated one step
	 * after the method has been called once.
	 */
	@Override
	public void update() {
		robot.move();
			
		int curr_reading = 0;
		current_reading = robot.getEstimatedPosition();
		
		int[] trueposition = getCurrentTruePosition();
		
		System.out.println("Current reading: " + current_reading[0] + " " + current_reading[1]);
		System.out.println("True position: " + trueposition[0] + " " + trueposition[1]);
		System.out.println("Distance between sensor report and true position:" + distance(current_reading, trueposition));
		
		
		if(current_reading[0] == -1){
			//this means that the sensor reported state nothing
			curr_reading = rows*cols;
		}else{
			//calculate the sensor coordinates position, ie position 0...rows*cols - 1
			curr_reading = (cols*current_reading[0]) + current_reading[1];
		}
		
		//create the diagonal matrix by fetching the elements in the row sensormodel[curr_reading]
		double O[][] = new double[rows*cols*head][rows*cols*head];
		for(int i = 0; i < rows*cols*head; i++){
			for(int j = 0; j < rows*cols*head; j++){
				if(i == j){
					O[i][j] = robot.getElementInSModel(curr_reading,j);
				}else{
					O[i][j] = 0.0;
				}
			}
		}

 		
 		//do the matrix calculations for f. 
		double f_new[] = new double[rows*cols*head];
 		double value[][] = new double[rows*cols*head][rows*cols*head];
 		for(int i = 0; i < rows*cols*head; i++){
 			for(int j = 0; j < rows*cols*head; j++){
 				for(int k = 0; k < rows*cols*head; k++){
 					value[i][j] += O[i][k]*robot.getElementInTModel(j,k);
 				}
 			}
 		}
 		double sum = 0.0;
 		for(int i = 0; i < rows*cols*head; i++){
 			for(int j = 0; j < rows*cols*head; j++){
 				sum += value[i][j]*f[j];
 				f_new[i] += value[i][j]*f[j];
 			}
 		}
 		for(int i = 0; i < rows*cols*head; i++){
 				f_new[i] /= sum;
 		}
 		f = f_new;
 		
	}

	private double distance(int[] current_reading, int[] trueposition) {
		// TODO Auto-generated method stub
		return Math.sqrt(Math.pow(current_reading[0] - trueposition[0], 2) + Math.pow(current_reading[1] - trueposition[1], 2));		
	}
	/*
	 * returns the currently known true position i.e., after one simulation step
	 * of the robot as (x,y)-pair.
	 */
	@Override
	public int[] getCurrentTruePosition() {
		return robot.getCurrentTruePosition();
	}


/*
	 * returns the currently available sensor reading obtained for the true position 
	 * after the simulation step 
	 */
	@Override
	public int[] getCurrentReading() {
		// TODO Auto-generated method stub
		return current_reading;
	}



	/*
	 * returns the currently estimated (summed) probability for the robot to be in position
	 * (x,y) in the grid. The different headings are not considered, as it makes the 
	 * view somewhat unclear.
	 */
	@Override
	public double getCurrentProb(int x, int y) {
		// TODO Auto-generated method stub
		double sum = 0.0;
		
		for (int h = 0; h<head; h++){
			int index = robot.toState(x, y, h);
			sum += f[index];
		}
		return sum;
	}

	/*
	 * returns the probability entry of the sensor matrices O to get reading r corresponding 
	 * to position (rX, rY) when actually in position (x, y) (note that you have to take 
	 * care of potentially necessary transformations from states i = <x, y, h> to 
	 * positions (x, y)). If rX or rY (or both) are -1, the method should return the probability for 
	 * the sensor to return "nothing" given the robot is in position (x, y).
	 

	The sensor reports
	 - the true location L with probability 0.1
	 - any of the n_Ls âˆˆ {3, 5, 8} existing surrounding fields L_s with probability 0.05 each
	 - any of the n_Ls2 âˆˆ {5, 6, 7, 9, 11, 16} existing â€œsecondaryâ€� surrounding fields L_s2 with probability 0.025 each
	 - "nothing" with probability 1.0 - 0.1 - n_Ls*0.05 - n_Ls2*0.025
	 
	This means that the sensor is more likely to produce â€œnothingâ€� when the robotâ€™s true
	location is less than two steps from a wall or in a corner (there are also other
	possibilities of interpreting the sensor readings, but you should stick to this model for
	the task).*/

	@Override
	public double getOrXY(int rX, int rY, int x, int y) {
		// TODO Auto-generated method stub
		int i = cols*rX + rY;
		int j = toState(x, y, 0);
		
		if(rX  == -1 || rY == -1){
			i = rows*cols;
		}		
		return robot.getElementInSModel(i,j);
	}
		

	/*
	 * returns the probability entry (Tij) of the transition matrix T to go from pose 
	 * i = (x, y, h) to pose j = (nX, nY, nH)
	 */
	
	public int toState(int x, int y, int h){
		return cols*head*x + head*y + h;
	}

	public int[] toCoordinate (int s){
		int[] ret = new int[3];					
		ret[0] = (int) Math.floor(s / (head*cols)); //new x
		ret[1] = (int) Math.floor(s % (cols*head) / head); //new y
		ret[2] = s % head;//new head
		return ret;
	}

	@Override	
	public double getTProb(int x, int y, int h, int nX, int nY, int nH) {		
		int i = toState(x, y, h);
		int j = toState(nX, nY, nH);
		return robot.getElementInTModel(i,j);
	}

}


