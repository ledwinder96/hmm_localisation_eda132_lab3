package model;

import java.util.Random;

public class Robot {
	private int state = 0; //current state of robot, 0 - 63. 
	private int curr_x;
	private int curr_y;
	private int curr_h;
	private double transitionmodel[][];
	private double sensormodel[][];
	private int rows;
	private int cols;
	private int head;
	private static int NORTH = 0;
	private static int EAST = 1;
	private static int SOUTH = 2;
	private static int WEST = 3;

	public Robot(int rows,int cols, int head){
		this.rows = rows;
		this.cols = cols;
		this.head = head;
		transitionmodel = new double[rows*cols*head][rows*cols*head];
		for(int i = 0; i < rows; i++){
			for (int j = 0; j<cols; j++){
				for (int k = 0; k<head; k++){
					fillTmodel(i,j,k);
				}
			}
		}
		
		//We have rows*cols positions + position nothing, therefore rows*cols + 1 rows.
		//sensormodel[r] gives us the O matrix for sensor reading r, represented as a vector. Sensormodel [r][i] gives us the 
		//probability for the sensor to report r while in state i 	
		sensormodel = new double[rows*cols + 1][rows*cols*head];
		
		
		for(int r = 0; r < rows*cols + 1; r++){
			for(int j = 0; j < rows*cols*head; j++){
					//sensormodel[r][j] = probability to report r when in state j.
				int position[] = toCoordinate(j);
				if(r != rows*cols){
					sensormodel[r][j] = calculateSProb(r / cols , r % cols, position[0], position[1]);
				}else{
					sensormodel[r][j] = calculateSProb(-1 , -1, position[0], position[1]);
				}
			}
		}
		
//		for(int i = 0; i < rows*cols + 1; i++){
//			System.out.println("");
//			for(int j = 0; j < rows*cols*head; j++){
//					System.out.print(sensormodel[i][j] + " ");
//			}
//		}
		curr_x = 0;
		curr_y= 0;
		curr_h = 0;
	}

	public void move(){
		// Find the state with highest probability
		// Update current x, y, h accordign to that state.

		int n_states = rows*cols*head;
		int current_state = toState(curr_x, curr_y, curr_h);
		
		int picked_state = -1;

		while(picked_state < 0){
			for (int i = 0; i<n_states; i++){
				double current_prob = transitionmodel[current_state][i]; //Given the current state, what is the probability that we move to state i?
				
				//We will basically "spela tärning" for each succesive move
				if(picked_state<0){ 				//If we haven't picked a state yet ...
					Random r = new Random();		//... 
					double rand = r.nextDouble();		//... then generate a random number rand between 0.0 and 1.0
					if (rand < current_prob)			//... and check if the random number is below the probability...
						picked_state = i;			//... If r is less than prob we pick this transition
				}									//... Meaning that if prob = 1 we are guaranteed to pick it and if prob = 0 we will never pick it
			}	
		}		
		int[] coordinates = toCoordinate(picked_state);
		//use coordinates to update curr_x, curr_y and curr_h
		curr_x = coordinates[0];
		curr_y = coordinates[1];
		curr_h = coordinates[2];			
	}

	public int toState(int x, int y, int h){
		return cols*head*x + head*y + h;
	}

	public int[] toCoordinate (int s){
		int[] ret = new int[3];					
		ret[0] = (int) Math.floor(s / (head*cols)); //new x
		ret[1] = (int) Math.floor(s % (cols*head) / head); //new y
		ret[2] = s % head;//new head
		return ret;
	}

	public void fillTmodel (int x, int y, int h){
			int curr_index = toState(x,y,h); 			
			double sum = 0.0;
			double directionchanged = 0;
			
			//check probabilities SOUTH
			int sim_index1 = 0;
			double prob1 = 0.0;
			boolean prob1_dir = false;
			if(x + 1 < rows){
				for(int i = 0; i < head; i++){				
					sim_index1 = toState(x + 1, y, i);
					prob1 = calculateTProb(x, y, h, x + 1, y, i);
					if(prob1 > 0.0){
						if(h != i){
							directionchanged++;
							prob1_dir = true;
						}
						break;
					}	
				}
			}
			
			//check probabilities NORTH
			int sim_index2 = 0;
			double prob2 = 0.0;
			boolean prob2_dir = false;
			if(x - 1 >= 0){
				for(int i = 0; i < head; i++){
					sim_index2 = toState(x-1,y,i);
					prob2 = calculateTProb(x, y, h, x - 1, y, i);
					if(prob2 > 0.0){
						if(h != i){
							directionchanged++;
							prob2_dir = true;
						}
						break;
					}	
				}
			}
			
			//check probabilities EAST
			int sim_index3 = 0;
			double prob3 = 0.0;
			boolean prob3_dir = false;
			if(y + 1 < cols){
				for(int i = 0; i < head; i++){
					sim_index3 = toState(x,y+1,i);
					prob3 = calculateTProb(x, y, h, x, y + 1, i);
					if(prob3 > 0.0){
						if(h != i){
							directionchanged++;
							prob3_dir = true;
						}
						break;
					}	
				}
			}
			
			//check probabilities WEST
			double prob4 = 0.0;
			int sim_index4 = 0;
			boolean prob4_dir = false;
			if(y - 1 >= 0){
				for(int i = 0; i < head; i++){
					sim_index4 = toState(x,y-1,i);
					prob4 = calculateTProb(x, y, h, x, y - 1, i);
					if(prob4 > 0.0){
						if(h != i){
							directionchanged++;
							prob4_dir = true;
						}
						break;
					}				
				}
			}
			
			
			if(prob1_dir)
				prob1 /= directionchanged;
			
			if(prob2_dir)
				prob2 /= directionchanged;
			
			if(prob3_dir)
				prob3 /= directionchanged;
			
			if(prob4_dir)
				prob4 /= directionchanged;
			
			if(x + 1 < rows){
				transitionmodel[curr_index][sim_index1] = prob1;
			}
			if(x - 1 >= 0){
				transitionmodel[curr_index][sim_index2] = prob2;
			}
			if(y + 1 < cols){
				transitionmodel[curr_index][sim_index3] = prob3;
			}
			if(y - 1 >= 0){
				transitionmodel[curr_index][sim_index4] = prob4;
			}			
	}

	public double getElementInTModel(int i, int j){
		return transitionmodel[i][j];
	}
	
	public double getElementInSModel(int i, int j) {
		return sensormodel[i][j];
	}
	
	public int[] getCurrentTruePosition() {
		int[] current = {curr_x, curr_y};
		return current;
	}

	public double calculateTProb(int x, int y, int h, int nX, int nY, int nH){
		double p = 0.0; // RETURN VALUE
		boolean wall = false;

		if ( (x == 0 && h == NORTH) || 
			 (x == rows - 1 && h == SOUTH) || 
			 (y == 0 && h == WEST) || 
			 (y == cols - 1 && h == EAST) ) {
			wall = true;
		}
		
		int diff_x = nX - x;
		int diff_y = nY - y;

		boolean move_x = ( (diff_x) != 0 );
		boolean move_y = ( (diff_y) != 0 );
		boolean not_moving = diff_x == 0 && diff_y == 0;
		boolean moving_diagonally = (move_x && move_y);

		if ( moving_diagonally || not_moving || (Math.abs(diff_x) > 1 || Math.abs(diff_y) > 1)) {
			//Diagonal move, not moving, or more than one step ahead
			return 0.0;
		}

		//At this point we know that the proposed movement is in one direction, or step, (either in x or y)

		if (diff_x > 0 && nH != SOUTH)		//If the x-movement is POSITIVE, we have to be moving SOUTH, otherwise return 0.0
			return 0.0;

		if (diff_x < 0 && nH != NORTH)		//If the x-movement is NEGATIVE, we have to be moving NORTH, otherwise return 0.0
			return 0.0;

		if (diff_y > 0 && nH != EAST)		//If the y-movement is POSITIVE, we have to be moving EAST, otherwise return 0.0
			return 0.0;

		if (diff_y < 0 && nH != WEST)		//If the y-movement is NEGATIVE, we have to be moving WEST, otherwise return 0.0
			return 0.0;

		
		//At this point we know that the heading is possible.

		/*	
			P( h_t+1 = h_t | not encountering a wall) = 0.7
			P( h_t+1 != h_t | not encountering a wall) = 0.3
			P( h_t+1 = h_t | encountering a wall) = 0.0
			P( h_t+1 != h_t | encountering a wall) = 1.0
		*/

		if (nH == h){
			p = 0.7;
			if (wall){
				p = 0.0;
			}
		}else{
			p = 0.3;
			if (wall) {
				p = 1.0;
			}
		}
		return p;
	}
	
	public double calculateSProb(int rX, int rY, int x, int y){
		int diff_x = Math.abs(rX-x);
		int diff_y = Math.abs(rY-y);
		double baseProb = 0.1;
		double n_Ls = 0.0;
		double n_Ls2 = 0.0;
		
		if ((x == 0 || y == 0 || x == rows - 1 || y == cols - 1)){
			//At a wall
			n_Ls = 5.0;
			n_Ls2 = 6.0;

			if ((x == 0 || x == rows - 1 ) && (y == 0 || y == cols - 1)){
				//At a corner
				n_Ls = 3.0;
				n_Ls2 = 5.0;
			}
		}else{
			//One of the four "Middle" positions
			n_Ls = 8.0;
			n_Ls = 7.0;
		}
		
		if (rX < 0 || rY < 0){
			return 1.0 - 0.1 - (n_Ls*0.05) - (n_Ls2*0.025);
		}

		for (int i =0; i<3; i++){
			if(diff_x <= i && diff_y <= i){
				return baseProb;
			}
			baseProb = ((double) baseProb)/2.0;
		}
		return 0.0;	
	}

	public int[] getEstimatedPosition() {
		// TODO Auto-generated method stub
		int n_positions = rows*cols + 1;
		int current_state = toState(curr_x, curr_y, curr_h);

		int picked_position = -1;
		while(picked_position < 0){
			for (int r = 0; r < n_positions; r++){			
				//returns the entry for current_state from diagonal matrix for sensor reading r. 
				//ie O[r][current_state] where r is the currently picked sensor reading. 
				double current_prob = sensormodel[r][current_state]; 
				//We will basically "spela tärning" for each succesive move
				if(picked_position<0){ 				//If we haven't picked a state yet ...
					Random random = new Random();		//... 
					double rand = random.nextDouble();		//... then generate a random number rand between 0.0 and 1.0
					if (rand < current_prob)			//... and check if the random number is below the probability...
						picked_position = r;			//... If r is less than prob we pick this transition
				}									//... Meaning that if prob = 1 we are guaranteed to pick it 												//and if prob = 0 we will never pick it
			}	
		}
		//System.out.println("picked position " + picked_position);
		int[] sensorreading  = {picked_position / cols ,  picked_position % cols};
		//om vi valt nothing. //Sista raden i sensormodel är chanserna att vi rapporterar nothing.
		if(picked_position == rows*cols){
			sensorreading[0] = -1;
			sensorreading[1] = -1;
		}	
			return sensorreading;					
	}	
}
